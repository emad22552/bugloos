<?php

namespace Emad\Bugloos\Casts;

use Emad\Bugloos\Models\Temp;
use Illuminate\Contracts\Database\Eloquent\CastsInboundAttributes;

class CustomCastObject implements CastsInboundAttributes
{
    /**
     *  create objects using other custom type casts.
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function set($model, string $key, $value, array $attributes)
    {
        // get data and config
        $apiDataRecord = $value['api_data_record'];
        $fieldConfig = $value['filed_config'];

        $tmp = []; // this will be returned.
        // for each object keys, based on user config.
        foreach ($fieldConfig['api_field'] as $jsonKey => $keyConfig){
            // this model is a temp but extended form Eloquent db Model, so has mergeFillable and mergeCasts methods.
            $model = new Temp;
            $tmp[$jsonKey] = $this->recursiveAttributeSetter($model, $jsonKey, $keyConfig, $apiDataRecord);
        }

        return json_encode($tmp, JSON_UNESCAPED_UNICODE);
    }

    /**
     * this a recursive method to create n-level depth objects based on user config.
     *
     * @param \Illuminate\Database\Eloquent\Model $model a temp model to use.
     * @param string $jsonKey
     * @param array $keyConfig
     * @param array $apiDataRecord
     * @return array|string
     */
    protected function recursiveAttributeSetter(\Illuminate\Database\Eloquent\Model $model, string $jsonKey, array $keyConfig, array $apiDataRecord){
        // if sub elements of object is also an object.
        if ($keyConfig['db_type'] instanceof self){
            $tmp = [];
            foreach ($keyConfig['api_field'] as $subJsonKey => $subKeyConfig){
                $subModel = new Temp;
                // get the element as a standalone object and call this func again.
                $tmp[$subJsonKey] = $this->recursiveAttributeSetter($subModel, $subJsonKey, $subKeyConfig, $apiDataRecord);
            }
            return $tmp;
        }

        // if element has a regular type and it uses other casting type.

        $model->mergeFillable([
            $jsonKey,
        ]);
        // set casting
        $model->mergeCasts([
            $jsonKey => $keyConfig['db_type'],
        ]);
        $model->$jsonKey = [
            'api_data_record' => $apiDataRecord,
            'filed_config' => $keyConfig,
        ];
        // get final value and return to set function.
        return $model->$jsonKey;
    }
}
