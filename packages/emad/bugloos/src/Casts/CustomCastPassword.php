<?php

namespace Emad\Bugloos\Casts;

use Illuminate\Contracts\Database\Eloquent\CastsInboundAttributes;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Hash;

class CustomCastPassword implements CastsInboundAttributes
{
    /**
     * Specific type casting for e.g. passwords
     *
     * @param  \Illuminate\Database\Eloquent\Model  $model
     * @param  string  $key
     * @param  mixed  $value
     * @param  array  $attributes
     * @return mixed
     */
    public function set($model, string $key, $value, array $attributes)
    {
        // get init data from passed value
        $apiDataRecord = $value['api_data_record'];
        $fieldConfig = $value['filed_config'];

        // get data value from passed record based on user config and make hash password out of it.
        return Hash::make(Arr::get($apiDataRecord, $fieldConfig['api_field']));
    }
}
