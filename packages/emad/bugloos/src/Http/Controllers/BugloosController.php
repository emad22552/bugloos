<?php
namespace Emad\Bugloos\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\File;
use Emad\Bugloos\Http\Resources\UserCollection;

class BugloosController
{
    /**
     * an endpoint to return data as json/xml.
     *
     * @param Request $request
     * @param string $format json|xml
     * @return Response
     */
    public function getData(Request $request, string $format): Response {
        $content = self::getFilesData($format);
        return response($content, 200, [
            'Content-Type' => "application/{$format}"
        ]);
    }


    /**
     * method to read data from resource files.
     *
     * @param string $format json|xml
     * @return string
     */
    private static function getFilesData(string $format) : string {
        $path = resource_path() . '/api_files/data.' . $format;
        return File::get($path);
    }

    /**
     * An endpoint to call and do conversion and all stuff.
     *
     * @param Request $request
     * @param string $format json|xml
     * @return UserCollection
     */
    public function handleDataConvert(Request $request, string $format): UserCollection {
        // get data from files
        $data = self::getFilesData($format);
        // if data type is xml, convert it to json.
        if ($format == 'xml') {
            $data = simplexml_load_string($data);
            $data = json_encode($data);
        }
        $data = json_decode($data, true)['results'];

        // use Api Resource to handle response.
        return new UserCollection($data);
    }
}
