<?php

namespace Emad\Bugloos\Http\Resources;

use Illuminate\Http\Resources\Json\ResourceCollection;
use Illuminate\Support\Collection;
use App\Models\User;
use Emad\Bugloos\Casts\CustomCastDefault;
use Emad\Bugloos\Casts\CustomCastObject;
use Emad\Bugloos\Casts\CustomCastPassword;

class UserCollection extends ResourceCollection
{
    // user api response and db fields type map
    private $apiParamsDbMap = [
        'first_name' => [
            'db_type' => CustomCastDefault::class,
            'api_field' => 'name.first',
        ],
        'last_name' => [
            'db_type' => CustomCastDefault::class,
            'api_field' => 'name.last',
        ],
        'address' => [
            'db_type' => CustomCastObject::class,
            'api_field' => [
                'country' => [
                    'db_type' => CustomCastDefault::class,
                    'api_field' => 'location.country',
                ],
                'city' => [
                    'db_type' => CustomCastDefault::class,
                    'api_field' => 'location.city',
                ],
                'coordinates' => [
                    'db_type' => CustomCastObject::class,
                    'api_field' => [
                        'lat' => [
                            'db_type' => CustomCastDefault::class,
                            'api_field' => 'location.coordinates.latitude'
                        ],
                        'long' => [
                            'db_type' => CustomCastDefault::class,
                            'api_field' => 'location.coordinates.longitude'
                        ],
                    ],
                ],
                'postcode' => [
                    'db_type' => CustomCastDefault::class,
                    'api_field' => 'location.postcode',
                ],
            ],
        ],
        'password' => [
            'db_type' => CustomCastPassword::class,
            'api_field' => 'login.password',
        ],
        'phone' => [
            'db_type' => CustomCastDefault::class,
            'api_field' => 'phone',
        ],
        'email' => [
            'db_type' => CustomCastDefault::class,
            'api_field' => 'email',
        ],
    ];

    /**
     * Transform the resource collection into an array.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return array|\Illuminate\Contracts\Support\Arrayable|\JsonSerializable
     */
    public function toArray($request)
    {
        // convert Collection and UserResource to regular array
        $data = json_decode(json_encode($this->collection->toArray()), true);
        $collection = Collection::empty();

        // for each data came from outer api call (our data files)
        foreach ($data as $item){
            $user = new User;
            // for each database field user wanted in config map
            foreach ($this->apiParamsDbMap as $fieldName => $config){
                // add that field to fillable
                $user->mergeFillable([
                    $fieldName,
                ]);
                // set model field casting based on user config
                $user->mergeCasts([
                    $fieldName => $config['db_type'],
                ]);
                // call castings and pass required data to it
                $user->$fieldName = [
                    'api_data_record' => $item,
                    'filed_config' => $config,
                ];
            }
            // here we can save to db if needed
            //$user->save();

            $collection->add($user);
        }

        return [
            'data' => $collection
        ];
    }
}
