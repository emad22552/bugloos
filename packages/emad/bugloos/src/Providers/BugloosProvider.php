<?php

namespace Emad\Bugloos\Providers;

use Illuminate\Support\Facades\Route;
//use Illuminate\Support\ServiceProvider;
use Illuminate\Foundation\Support\Providers\RouteServiceProvider as ServiceProvider;


class BugloosProvider extends ServiceProvider
{
    /**
     * Bootstrap services.
     *
     * @return void
     */
    public function boot()
    {
        // register routes
        $this->routes(function () {
            Route::prefix('api/v1')
                ->namespace($this->namespace)
                ->group((__DIR__.'/../routes/api_v1.php'));
        });


    }
}
