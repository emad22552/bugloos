<?php

use Illuminate\Support\Facades\Route;
use Emad\Bugloos\Http\Controllers\BugloosController;

// route to get and see data in json/xml format
Route::get('/get-data/{format}', [BugloosController::class, 'getData'])
    ->where('format', 'json|xml')
    ->name('get-data');

// route to do conversion and all stuff
Route::get('/handle_data_convert/{format}', [BugloosController::class, 'handleDataConvert'])
    ->where('format', 'json|xml');

